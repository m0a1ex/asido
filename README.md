[appasset/asido][@]
===================

Overview
--------

__Asido__ is a __PHP__ *(PHP4/PHP5)* image processing solution, with `pluggable` drivers *(adapters)* for virtually any environment: __GD2__, __Magick Wand__, __Image Magick via shell__, __Image Magick via extension__, etc.



[@]:  https://github.com/appasset/asido "appasset/asido"
